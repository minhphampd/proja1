// Original project by Gerard Llorach (2014)
// Updated by Oliver Dawkins and Dominic Zisch (2017) to visualise points using height and intensity gradients
//modified by Simone Milani 2020 for 3DAR 20/21

#pragma warning disable 0168

using UnityEngine;
using System.Collections;
using System.IO;

public class ReadMesh : MonoBehaviour
{

	// File location
	public string dataPath;
	private string filename;
	public Material matVertex;

	// Methods to colour points
	public enum cpb { Default, RGB, Height, Intensity };
	private cpb colourPointsBy = cpb.RGB;
	private Color defaultPointColour;
	private Gradient colourGradient;

	// Processing GUI
	private float progress = 0;
	private new string guiText;
	private bool loaded = false;

	// Point cloud properties
	private GameObject pointCloud;

	public float scale = 1;
	public bool relocateToOrigin = true;
	//private bool invertYZ = false;
	public bool forceReload = true;

	public int numPoints;
	public int numPointGroups = 300;
	private int limitPoints;

	private Vector3[] points;
	private Color[] colors;
	private Vector3 minValue;

	// Point height properties
	private float minHeight;
	private float maxHeight;
	private float heightDiff;
	private float localDiff;

	// Point intensity properties
	private float minIntensity;
	private float maxIntensity;
	private float intensityDiff;
	private float relativeDiff;
	private string currentDirectory;
	private string currentBaseDirectory;

	public float colorCoefficientRed;
	public float colorCoefficientGreen;
	public float colorCoefficientBlue;

	//public GameObject PointPrefab;



	void Start()
	{
		
		//Calculate height difference for the visualising height gradient
		heightDiff = maxHeight - minHeight;

		//Calculate intensity difference for visualising intensity gradient
		intensityDiff = maxIntensity - minIntensity;

		currentDirectory = Application.dataPath;
		currentDirectory = currentDirectory.Replace(@"\", "/");
		bool hasFoundMatch = false;

		if (!currentDirectory.EndsWith("/"))
			currentDirectory += "/";

		switch (Application.platform)
		{
			case RuntimePlatform.OSXEditor: //<path to project folder>/Assets
			case RuntimePlatform.WindowsEditor:
				if (currentDirectory.EndsWith("Assets/"))
				{
					currentDirectory = currentDirectory.Substring(0, currentDirectory.LastIndexOf("Assets/"));
					currentDirectory += "RuntimeData/";
					hasFoundMatch = true;
				}
				break;
			case RuntimePlatform.WindowsPlayer: //<path to executablename_Data folder>
				break;
			case RuntimePlatform.OSXPlayer: //<path to player app bundle>/Contents
				if (currentDirectory.EndsWith(".app/Contents/"))
				{
					currentDirectory = currentDirectory.Substring(0, currentDirectory.LastIndexOf(".app/Contents/"));
					currentDirectory += "RuntimeData/";
					hasFoundMatch = true;
				}
				break;
			default:
				hasFoundMatch = false;
				break;
		}

		Debug.Log(currentDirectory);

		if (!hasFoundMatch)
		{
			currentDirectory = Path.GetFullPath("RuntimeData/");
			currentDirectory = currentDirectory.Replace(@"\", "/");
		}

		if (!Directory.Exists(currentDirectory))
		{
			for (int i = 0; i < 3; i++)
				currentDirectory = currentDirectory.Substring(0, currentDirectory.LastIndexOf("/"));
			currentDirectory += "/RuntimeData/";
		}

		currentBaseDirectory = currentDirectory.Replace("/RuntimeData", "");
		Debug.Log(currentBaseDirectory);

		var lines = File.ReadAllLines(currentBaseDirectory + "/Project/Assets/Models/" + dataPath + ".xyz");

		Debug.Log("All fine");

		// Create Resources folder
		//createFolders();

		// Get Filename
		filename = Path.GetFileName(currentBaseDirectory + "/Project/Assets/Models/" + dataPath + ".xyz");
					
		loadPointCloud();
	}

	void loadScene()
	{
		// Check if the PointCloud was loaded previously
		loadPointCloud();
	}

	void loadPointCloud()
	{
		// Check what file exists
		if (File.Exists(currentBaseDirectory + "/Project/Assets/Models/" + dataPath + ".xyz"))
		{
			Debug.Log(dataPath);
			// Load XYZ
			StartCoroutine("loadXYZ", "/Models/" + dataPath + ".xyz");
		}
		else
			Debug.Log("File '" + currentBaseDirectory + "/Project/Assets/Models/" + dataPath + "' could not be found");

	}

	// Load stored PointCloud
	void loadStoredMeshes()
	{

		Debug.Log("Using previously loaded PointCloud: " + filename);

		GameObject pointGroup = Instantiate(Resources.Load("PointCloudMeshes/" + filename)) as GameObject;

		loaded = true;
	}

	// Start Coroutine of reading the points from the XYZ file and creating the meshes
	IEnumerator loadXYZ(string dPath)
	{

		// Read file
		numPoints = File.ReadAllLines(Application.dataPath + dPath).Length;
		
			StreamReader sr = new StreamReader(Application.dataPath + dPath);

		points = new Vector3[numPoints];
		colors = new Color[numPoints];
		minValue = new Vector3();

		for (int i = 0; i < numPoints; i++)
		{
			string[] buffer = sr.ReadLine().Split(',');
			float x = float.Parse(buffer[0]) * scale;
			float y = float.Parse(buffer[1]) * scale;
			float z = float.Parse(buffer[2]) * scale;

            points[i] = new Vector3(x, y, z);



			// Colour points by RGB values

			int R = int.Parse(buffer[3]);
			int G = int.Parse(buffer[4]);
			int B = int.Parse(buffer[5]);

			
			colors[i] = new Color(R / colorCoefficientRed, G/ colorCoefficientGreen, B/ colorCoefficientBlue);

	
           
			// Processing GUI
			progress = i * 1.0f / (numPoints - 1) * 1.0f;
            if (i % Mathf.FloorToInt(numPoints / 20) == 0)
            {
                guiText = i.ToString() + " out of " + numPoints.ToString() + " loaded";
                yield return null;
            }
        }

		// Instantiate Point Groups
		limitPoints = Mathf.CeilToInt(numPoints * 1.0f / numPointGroups * 1.0f);
		
		Debug.Log("Points group #: " + numPointGroups);
		pointCloud = new GameObject(filename);
		print(filename);
		for (int i = 0; i < numPointGroups - 1; i++)
		{
			InstantiateMesh(i, limitPoints);
            if (i % 10 == 0)
            {
                guiText = i.ToString() + " out of " + numPointGroups.ToString() + " PointGroups loaded";
                yield return null;
            }
        }
		
		InstantiateMesh(numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints);
		//Store PointCloud
		UnityEditor.PrefabUtility.SaveAsPrefabAsset(pointCloud, "Assets/Resources/PointCloudMeshes/" + filename + ".prefab");

		loaded = true;
		
	}

	void InstantiateMesh(int meshInd, int nPoints)
	{
		// Create Mesh
		GameObject pointGroup = new GameObject(filename + meshInd);
		pointGroup.AddComponent<MeshFilter>();
		pointGroup.AddComponent<MeshRenderer>();
		pointGroup.GetComponent<Renderer>().material = matVertex;

		pointGroup.GetComponent<MeshFilter>().mesh = CreateMesh(meshInd, nPoints, limitPoints);
		pointGroup.transform.parent = pointCloud.transform;

		// Store Mesh
		UnityEditor.AssetDatabase.CreateAsset(pointGroup.GetComponent<MeshFilter>().mesh, "Assets/Resources/PointCloudMeshes/" + filename + meshInd + ".asset");
		UnityEditor.AssetDatabase.SaveAssets();
		UnityEditor.AssetDatabase.Refresh();
	}

	Mesh CreateMesh(int id, int nPoints, int limitPoints)
	{

		Mesh mesh = new Mesh();

		Vector3[] myPoints = new Vector3[nPoints];
		int[] indecies = new int[nPoints];
		Color[] myColors = new Color[nPoints];

		for (int i = 0; i < nPoints; ++i)
		{
			myPoints[i] = points[id * limitPoints + i] - minValue;
			indecies[i] = i;
			myColors[i] = colors[id * limitPoints + i];
		}


		mesh.vertices = myPoints;
		mesh.colors = myColors;
		mesh.SetIndices(indecies, MeshTopology.Points, 0);
		mesh.uv = new Vector2[nPoints];
		mesh.normals = new Vector3[nPoints];


		return mesh;
	}

	void calculateMin(Vector3 point)
	{
		if (minValue.magnitude == 0)
			minValue = point;


		if (point.x < minValue.x)
			minValue.x = point.x;
		if (point.y < minValue.y)
			minValue.y = point.y;
		if (point.z < minValue.z)
			minValue.z = point.z;
	}

	void createFolders()
	{
		if (!Directory.Exists(Application.dataPath + "Resources/"))
			UnityEditor.AssetDatabase.CreateFolder("Assets", "Resources");

		if (!Directory.Exists(Application.dataPath + "Resources/PointCloudMeshes/"))
			UnityEditor.AssetDatabase.CreateFolder("Assets/Resources", "PointCloudMeshes");
	}

	void OnGUI()
	{


		if (!loaded)
		{
			GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2, 400.0f, 20));
			GUI.Box(new Rect(0, 0, 200.0f, 20.0f), guiText);
			GUI.Box(new Rect(0, 0, progress * 200.0f, 20), "");
			GUI.EndGroup();
		}
	}
	void Update()
	{
		if (Input.GetKey(KeyCode.P))
		{
			StartCoroutine("loadXYZ", "/Models/" + dataPath + ".xyz");
			print("Update");
		}

	}

}
