#Code to downsampling and create mesh- Project A1 - Bui Mai Quynh Linh - Pham Quang Minh
# In this code, we use open3D library. 
import pandas as pd
import open3d as o3d
import numpy as np

# Read the mesh.xyzrgb, which contains the value of coordinate and color:Red, Green, Blue 
pcd = o3d.io.read_point_cloud('mesh.xyzrgb')

# Read the mesh.xyzn, which contains the coordinate and normals
normpcd = o3d.io.read_point_cloud('mesh.xyzn')

print(pcd) #show point number

# Step 1: Down sampling
downpcd = pcd.voxel_down_sample(voxel_size=0.05)
norm1pcd = normpcd.voxel_down_sample(voxel_size=0.05)
o3d.visualization.draw_geometries([downpcd]) #visualize the model

# Step 2: Add normal
downpcd.normals = norm1pcd.normals

# Step 3: Triangle mesh poisson
with o3d.utility.VerbosityContextManager(
        o3d.utility.VerbosityLevel.Debug) as cm:
    mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
        downpcd, depth=9)
print(mesh)
o3d.visualization.draw_geometries([mesh],
                                  zoom=0.664,
                                  front=[-0.4761, -0.4698, -0.7434],
                                  lookat=[1.8900, 3.2596, 0.9284],
                                  up=[0.2304, -0.8825, 0.4101])
# Step 4: Saving with vertex color
o3d.io.write_triangle_mesh('carnewfull.ply', mesh, write_ascii=False, compressed=False, write_vertex_normals=True, write_vertex_colors=True, write_triangle_uvs=True, print_progress=True)
