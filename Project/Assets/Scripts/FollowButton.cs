﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowButton : MonoBehaviour
{
    public float moveSpeed = 20;
	public float rotateSpeed = 30;
	private int boundary = 10;
	private int width;
	private int height;

	public Camera camera;
	private float xOffSet;
	private float yOffSet;
	private float zOffSet;

	public Vector3 centerRotationPoint;

	// Start is called before the first frame update
	void Start()
    {
        width = Screen.width;
        height = Screen.height;

		xOffSet = 0.0f;
		yOffSet = 0.0f;
		zOffSet = 0.0f;

		Camera.main.transform.Translate(xOffSet, yOffSet, zOffSet);
	}

    // Update is called once per frame
    void Update()
    {
		float horizontalValue = Input.GetAxis("Horizontal");
		float verticalValue = Input.GetAxis("Vertical");

		if (horizontalValue > width - boundary)
		{
			transform.position -= new Vector3(Input.GetAxisRaw("Horizontal") * Time.deltaTime * moveSpeed, 0.0f, 0.0f);
		}

		if (horizontalValue < 0 + boundary)
		{
			transform.position -= new Vector3(Input.GetAxisRaw("Horizontal") * Time.deltaTime * moveSpeed, 0.0f, 0.0f);
		}

		if (verticalValue > height - boundary)
		{
			transform.position -= new Vector3(0.0f,Input.GetAxisRaw("Vertical") * Time.deltaTime * moveSpeed, 0.0f);
		}

		if (verticalValue < 0 + boundary)
		{
			transform.position -= new Vector3(0.0f, Input.GetAxisRaw("Vertical") * Time.deltaTime * moveSpeed, 0.0f);
		}

		if (Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			zOffSet = Input.GetAxis("Mouse ScrollWheel");
			Camera.main.transform.Translate(xOffSet, yOffSet, zOffSet);
		}

		if (Input.GetKey(KeyCode.H))
		{
			transform.RotateAround(centerRotationPoint, Vector3.left, rotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.K))
		{
			transform.RotateAround(centerRotationPoint, Vector3.right, rotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.U))
		{
			transform.RotateAround(centerRotationPoint, Vector3.up, rotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.J))
		{
			transform.RotateAround(centerRotationPoint, Vector3.down, rotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.N))
		{
			transform.RotateAround(centerRotationPoint, Vector3.back, rotateSpeed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.M))
		{
			transform.RotateAround(centerRotationPoint, Vector3.forward, rotateSpeed * Time.deltaTime);
		}
	}
}
