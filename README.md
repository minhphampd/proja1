# Project A1 - 3DAR - 20/21 #

2020 Visualization Challenge: physical properties for mechanical models

### Steps to run ###
1. Project is into the ./Project folder
2. Open Unity 2020 then add project folder
3. Find the ./Project/Assets/Scenes/SampleScene.unity to load the scene
4. Tick/Untick each object which needs to be view
5. Set the properties for the object then play
6. Use mouse and keyboard to move, rotate and zoom the running object

### Movement keys ###
- Up, Down, Left, Right: Moving
- Mouse Wheel Scroll: Zoom
- U,J,H,K,N,M: Rotating

### Data Process ###
- Python 3.7 and the bibraries Pandas, Numpy, Open3D
- Matlab installation

### Additional tools ###
- Blender: https://www.blender.org/download/
- RGB Calculator: https://www.w3schools.com/colors/colors_rgb.asp