clear all;
close all;

fp=fopen('mesh_simple.bin','rb');
%read number of triangles
Ntri=fread(fp,[1 1],'int32');

%read triangle data
tri_mat=fread(fp,[3 Ntri],'int32');

%dummy strings to read (and throw away)
str=fread(fp,[1 8],'char');
char(str)
str=fread(fp,[1 32],'char');
char(str)

%read minimum pressure (not used)
min_v=fread(fp,[1 1],'float');
%read maximum pressure (not used)
max_v=fread(fp,[1 1],'float');

%read number of vertices
Nver = fread(fp,[1 1],'int32');

%read vertex values (7 fields per vertex)
%[ x y z nx ny nz pressure ]
ver_mat=fread(fp,[7 Nver],'float');

fclose(fp);

%define colormap


fp=fopen('mesh_simple.xyzrgb','w');

%quantize the values for visualization
delta=(max(ver_mat(7,:))-min(ver_mat(7,:)))/256+0.0001;
%quatization index (0-255)
yq=floor((ver_mat(7,:)-min(ver_mat(7,:)))/delta)+1;
%colormap
cmap=colormap;

%show the model
% figure(1);
% scatter3(ver_mat(4,:),ver_mat(5,:),ver_mat(6,:),ones(length(yq),1),...
%     cmap(yq,:),'filled');
% axis equal;


figure(1);

delta=(max(ver_mat(7,:))-min(ver_mat(7,:)))/256+0.0001;
% quatization index (0-255)

yq=floor((ver_mat(7,:)-min(ver_mat(7,:)))/delta)+1;
colormap;
cmap=colormap;
% show the model
for t=1:Nver
    a = cmap(yq(t),:);
    fprintf(fp,'%.6f %.6f %.6f %.6f %.6f %.6f\n',ver_mat(1,t),ver_mat(2,t),ver_mat(3,t),a);
end
%     for t=1:277730
%     delta=(max(ver_mat(7,t))-min(ver_mat(7,t)))/256+0.0001;
%     yq=floor((ver_mat(7,t)-min(ver_mat(7,t)))/delta)+1;
%     fprintf(fp,'%.6f,%.6f,%.6f,%d,%d,%d\n',ver_mat(1,t),ver_mat(2,t),ver_mat(3,t),round(cmap(yq,:)*255));
% end
fclose(fp);

