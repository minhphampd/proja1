clear all;
close all;

fp=fopen('flowlines.bin','rb');
%read number of flows
Nflows=fread(fp,[1 1],'int32');
%read dummy strings
str=fread(fp,[1 32],'char');
char(str)
%read minimum and maximum values for velocity
min_v=fread(fp,[1 1],'float');
max_v=fread(fp,[1 1],'float');
%read the number of vertices for all flowlines (not used)
Ntimes=fread(fp,[1 1],'int32');
%the max number of instants is this
Ntimes=125;

%create a matrix to store flows
flow_mat=zeros(Nflows,5,Ntimes);

for c=1:Nflows
    str=fread(fp,[1 8],'char');  %read dummy string
    %read number of instants (redundant) = Ntimes
    n1=fread(fp,[1 1],'int32');
    fff=fread(fp,[5 n1],'float');
    flow_mat(c,:,1:n1)=shiftdim(fff,-1);
end;

fclose(fp);

%define colormap
cmap=colormap;
cmap=cmap(round(linspace(1,156,35)),:);  %34 values used

fp=fopen('flows_ata.xyz','w');

figure(1);
for t=1:Ntimes

fff=flow_mat(:,1:3,t)'; 

for c=1:603
    i=floor(flow_mat(c,5,t))+1;
    fprintf(fp,'%.6f,%.6f,%.6f,%d,%d,%d\n',fff(:,c),round(cmap(i,1:3)*255));
end;

    
if t==1
    plot3(fff(1,:),fff(2,:),fff(3,:),'b.');
else
    hold on;
    plot3(fff(1,:),fff(2,:),fff(3,:),'b.');
    hold off;
end;

pause(0.1);

end;

fclose(fp);